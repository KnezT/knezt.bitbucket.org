
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	var ehrId;

	var sessionId = getSessionId();
	
	//datum
	var dan;
	var mesec;
	var leto;
	
	switch (stPacienta) {
		case 1:
			var ime = "Franc";
			var priimek ="Novak";
			var spol = "M";
			var datumRojstva = "1997-05-05";
			leto=1997;
			mesec=5;
			dan=5;
			break;
		case 2:
			var ime = "Marija";
			var priimek ="Novak";
			var spol = "Z";
			var datumRojstva = "1910-03-01";
			leto=1910;
			mesec=3;
			dan=1;
			break;
		default:
			var ime = "Janez";
			var priimek ="Novak";
			var spol = "M";
			var datumRojstva = "2005-12-07";
			leto=2005;
			mesec=12;
			dan=7;
	}
	

	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	spol=(spol=="M" ? "MALE" : "FEMALE");
	$.ajax({
		async: false,
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            gender: spol,
	            dateOfBirth: datumRojstva,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	        	async: false,
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                }
	            },
	            error: function(err) {
	            	console.log("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	            }
	        });
	    }
	});
	
	

		var visina=Math.floor((Math.random() * 30) + 40);//zacetna visina otroka
		var teza=Math.floor((Math.random() * 5) + 5);//zacetna teza otroka
		//trenutni datum
		var a=-Math.random()/10.0;
		var b=2+Math.random();
		for(var i=0;i<10;i++){
			var trenutnaVisina=a*i*i+b*i+visina;
			
			var trenutnaTeza=Math.floor((Math.random() * 2) - 1)+teza+i*0.3;
			shraniMeritev(ehrId,leto+"-"+mesec+"-"+dan,Math.round(trenutnaVisina)+"",Math.round(trenutnaTeza)+"");
			mesec++;
			if(mesec>12){
				mesec-=12;
				leto++;
			}
		}
		return ehrId;

	
}


function ustvarjanjeOseb(){
	alert("Generiranje podatkov traja nekaj sekund.\nProsim, da gumba ne pritiskate večkrat zapored.")
	var ehr1=generirajPodatke(1);
	var ehr2=generirajPodatke(2);
	var ehr3=generirajPodatke(3);
	$("#primerDodajanjaMeritve").html("<option value=''</option><option value='"+ehr1+",110,15,1999-05-05'>Franc Novak</option><option value='"+ehr2+",130,30,1915-03-01'>Marija Novak</option><option value='"+ehr3+",125,40,2015-12-07'>Janez Novak</option>");
	$("#primerPrikazaPodatkov").html("<option value=''></option><option value='"+ehr1+"'>Franc Novak</option><option value='"+ehr2+"'>Marija Novak</option><option value='"+ehr3+"'>Janez Novak</option>");
	$("#primerGrafa").html("<option value=''></option><option value='"+ehr1+"'>Franc Novak</option><option value='"+ehr2+"'>Marija Novak</option><option value='"+ehr3+"'>Janez Novak</option>");

		alert("ustvarjeni EhrId:\n"+ehr1+"\n"+ehr2+"\n"+ehr3);

}

$( document ).ready(prikaziSalo());

function prikaziSalo(){
	$.ajax({
		url: "https://api.icndb.com/jokes/random",
		type: 'POST',
		success: function(data){
			$("#sala").html(data.value.joke);
		}
	});
}

function novEhrZaOtroka() {
	var sessionId = getSessionId();

	var ime = $("#ime").val();
	var priimek = $("#priimek").val();
	var spol = $("#spol").val();
	var datumRojstva = $("#datumRojstva").val();

	if (!ime || !priimek || !spol || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || spol.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else if(spol!="M"&&spol!="Z"){
	    $("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Vneseni spol ni veljaven. Vnesite 'M' ali 'Z'.</span>");
	}else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		spol=(spol=="M" ? "MALE" : "FEMALE");
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#vnosEhrId").val(ehrId);
		                    $("#prikazEhrId").val(ehrId);
		                    $("#grafEhrId").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function dodajMeritev() {
	var sessionId = getSessionId();

	var ehrId = $("#vnosEhrId").val();
	var datum = $("#vnosDatum").val();
	var visina = $("#vnosVisina").val();
	var teza = $("#vnosTeza").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datum,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#sporociloDodajanjeMeritev").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Meritev je uspešno dodana.</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}
function shraniMeritev(ehrId, datum, visina, teza){
	//skoraj isto kot dodaj meritev
	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	var podatki = {
		// Struktura predloge je na voljo na naslednjem spletnem naslovu:
  // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": datum,
	    "vital_signs/height_length/any_event/body_height_length": visina,
	    "vital_signs/body_weight/any_event/body_weight": teza,
	};
	var parametriZahteve = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	};
	$.ajax({
		async: false,
	    url: baseUrl + "/composition?" + $.param(parametriZahteve),
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(podatki),
	    success: function (res) {
	        
	    },
	    error: function(err) {
	    	console.log("Napaka pri dodajanju podatkov:'" +JSON.parse(err.responseText).userMessage + "'!");
	    }
	});
}

var sessionId;//trenutno prikazani podatki
var ehrId;
function prikaziMeritve() {
	$("#sporociloPrikaz").html("");
	sessionId = getSessionId();

	ehrId = $("#prikazEhrId").val();

	if (ehrId.trim().length == 0) {
		$("#sporociloPrikaz").html("<span class='obvestilo " +
      "label label-warning fade-in'>Vnesite EHR ID!");
	} else {
		$("#prikazMeritev").html("");
		$.ajax({
  		    url: baseUrl + "/view/" + ehrId + "/" + "height",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	if (res.length > 0) {
			    	var results = "<table class='table table-striped table-hover'><tr><th>Datum meritve</th></tr>";
			        for (var i in res) {
			            results += "<tr><td id='"+i+"'>" + res[i].time.substring(0,10) +"<table class='table'></table></td></tr>";
			        }
			        results += "</table>";
			        $("#prikazMeritev").html(results);
		    	} else {
		    		$("#sporociloPrikaz").html("<span class='obvestilo label label-warning fade-in'>Ni podatkov!</span>");
		    	}
		    },
		    error: function(err) {
		    	$("#sporociloPrikaz").html(
    			"<span class='obvestilo label label-danger fade-in'>Napaka '" +
    			 JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
			
	}
}
$(document).ready(function(){
	$("#sporociloPrikaz").html("");
	$('#prikazMeritev').click(function() {
		var id=event.target.id
		sessionId=getSessionId();
		if(id.length!=0){
			if($("#"+id).children(0).text().length==0){
				
				var visina,teza;
				$.ajax({
  		    		url: baseUrl + "/view/" + ehrId + "/" + "height",
		    		type: 'GET',
		    		headers: {"Ehr-Session": sessionId},
		    		success: function (res) {
				        visina=res[parseInt(id)].height;
				        
				    },
				    error: function(err) {
				    	$("#sporociloPrikaz").html(
		    			"<span class='obvestilo label label-danger fade-in'>Napaka '" +
		    			 JSON.parse(err.responseText).userMessage + "'!");
		    			 
				    }
				});
				
				$.ajax({
  		    		url: baseUrl + "/view/" + ehrId + "/" + "weight",
		    		type: 'GET',
		    		headers: {"Ehr-Session": sessionId},
		    		success: function (res) {
				        teza= res[parseInt(id)].weight;
				        
				    },
				    error: function(err) {
				    	$("#sporociloPrikaz").html(
		    			"<span class='obvestilo label label-danger fade-in'>Napaka '" +
		    			 JSON.parse(err.responseText).userMessage + "'!");
		    			 
				    }
				});
				$(document).ajaxStop(function () {
					$("#"+id).children().html("<tr><th>Višina</th><th>Teža</th><th>BMI</th></tr><tr><td>" + visina + "cm</td><td>" + teza + "kg</td><td>" + Math.round(teza/((visina*visina)/10000)) + "</td></tr>");
					$(this).unbind("ajaxStop");
				});
			}
			else{
				$("#"+id).children().html("");
			}
		}
    });
    
    
    //spustni seznami
    $('#primerDodajanjaOtroka').change(function() {
	    $("#kreirajSporocilo").html("");
	    var podatki = $(this).val().split(",");
	    $("#ime").val(podatki[0]);
	    $("#priimek").val(podatki[1]);
	    $("#spol").val(podatki[2]);
	    $("#datumRojstva").val(podatki[3]);
	});
	
	$('#primerDodajanjaMeritve').change(function() {
	    $("#sporociloDodajanjeMeritev").html("");
	    var podatki = $(this).val().split(",");
	    $("#vnosEhrId").val(podatki[0]);
	    $("#vnosVisina").val(podatki[1]);
	    $("#vnosTeza").val(podatki[2]);
	    $("#vnosDatum").val(podatki[3]);
	});
	
	$('#primerPrikazaPodatkov').change(function() {
	    $("#sporociloPrikaz").html("");
	    var podatki = $(this).val();
	    $("#prikazEhrId").val(podatki);
	});
	
	$('#primerGrafa').change(function() {
	    $("#sporociloGraf").html("");
	    var podatki = $(this).val();
	    $("#grafEhrId").val(podatki);
	});
})


function narisiGraf(data,barva,tip){
	var svg = d3.select("svg"),
	    margin = {top: 20, right: 20, bottom: 30, left: 50},
	    width = +svg.attr("width") - margin.left - margin.right,
	    height = +svg.attr("height") - margin.top - margin.bottom,
	    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	var parseTime = d3.timeParse("%d.%m.%Y");
	
	var x = d3.scaleLinear()
	    .rangeRound([0, width]);
	
	var y = d3.scaleLinear()
	    .rangeRound([height, 0]);
	
	var line = d3.line()
	    .x(function(d) { return x(d.date); })
	    .y(function(d) { return y(d.close); });
	
	
	function narisi(data,barva,tip){
		console.log(data);
	  x.domain(d3.extent(data, function(d) { return d.date; }));
	  y.domain(d3.extent(data, function(d) { return d.close; }));
	
	  g.append("g")
	    .attr("transform", "translate(0," + height + ")")
	    .call(d3.axisBottom(x))
	    .select(".domain");
	    
	
	  
	  g.append("g")
	    .call(d3.axisLeft(y))
	    .append("text")
	    .attr("fill", "#000")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 6)
	    .attr("dy", "0.71em")
	    .attr("text-anchor", "end")
	    .text(tip);
	  
	      
	  g.append("path")
	      .datum(data)
	      .attr("fill", "none")
	      .attr("stroke", barva)//barva:steelblue
	      .attr("stroke-linejoin", "round")
	      .attr("stroke-linecap", "round")
	      .attr("stroke-width", 1.5)
	      .attr("d", line);
	}
	narisi(data,barva,tip);
}

function ustvariGraf(){
	var ehrId=$("#grafEhrId").val();
	if (ehrId.trim().length == 0) {
		$("#sporociloGraf").html("<span class='obvestilo label label-warning fade-in'>Vnesite EHR ID!");
		$("#graf").html("");
		return;
	}
	$("#graf").html("<svg width='500' height='300'></svg>");
	var dat=[];
	var nacin=$("#tipGrafa").val();
	
	$("#sporociloGraf").html("");
	sessionId=getSessionId();
	$.ajax({
  		url: baseUrl + "/view/" + ehrId + "/" + "height",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
			for(var i in res){
				dat[i]={};
				dat[i].visina=res[i].height;
				var leto=parseInt(res[i].time.substring(0,4));
				var mesec=parseInt(res[i].time.substring(5,7));
				var dan=parseInt(res[i].time.substring(8,10));
				dat[i].date=leto+(mesec/12.0)+(dan/365.0);
			}
	    },
	    error: function(err) {
	    	$("#sporociloGraf").html(
			"<span class='obvestilo label label-danger fade-in'>Napaka '" +
			 JSON.parse(err.responseText).userMessage + "'!");
			 
	    }
	});
	$(document).ajaxStop(function () {
		$.ajax({
	  		url: baseUrl + "/view/" + ehrId + "/" + "weight",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
			success: function (res) {
		        for(var i in res){
					dat[i].teza=res[i].weight;
				}
		    },
		    error: function(err) {
		    	$("#sporociloGraf").html(
				"<span class='obvestilo label label-danger fade-in'>Napaka '" +
				 JSON.parse(err.responseText).userMessage + "'!");
				 
		    }
		});
		$(this).unbind("ajaxStop");
		$(document).ajaxStop(function () {
			for(var i in dat){
				if(nacin=="Visina"){
					dat[i].close=dat[i].visina;
				}
				if(nacin=="Teza"){
					dat[i].close=dat[i].teza;
				}
				if(nacin=="BMI"){
					dat[i].close=Math.round(dat[i].teza/((dat[i].visina*dat[i].visina)/10000));
				}
			}
			narisiGraf(dat,"steelblue",nacin);
			$(this).unbind("ajaxStop");
		});
	});
	
	
	
}